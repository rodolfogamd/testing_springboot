# testing_springboot

Testing build and run with docker

Will spin up a maven container to build and a openjdk container to run the application

To run the containers:

`docker-compose up -d`

To login into containers

`docker-compose exec [service] sh`